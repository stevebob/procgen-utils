pub use coord_2d::Size;
use rand::{Rng, SeedableRng};
pub use rand_isaac::Isaac64Rng;
pub use simon::Arg;
use simon::*;

pub struct Args {
    pub size: Size,
    pub rng: Isaac64Rng,
}

impl Args {
    pub fn arg() -> impl Arg<Item = Self> {
        args_map! {
            let {
                rng_seed = opt::<u64>("r", "rng-seed", "rng seed", "INT")
                    .with_default_lazy(|| rand::thread_rng().gen());
                size = opt::<u32>("x", "width", "width", "INT").with_default(80)
                        .both(opt::<u32>("y", "height", "height", "INT").with_default(40))
                        .map(|(width, height)| Size::new(width, height));
            } in {{
                println!("RNG Seed: {}", rng_seed);
                let rng = Isaac64Rng::seed_from_u64(rng_seed);
                Self {
                    rng,
                    size,
                }
            }}
        }
    }
}
